import campaigns from '../dummyData/campaigns.json';

import {
    FETCH_CAMPAIGNS,
    FILTER_CAMPAIGNS,
    CREATE_CAMPAIGN
} from "../actionTypes";

export function fetchCampaigns(){
    return {
        type: FETCH_CAMPAIGNS,
        payload: campaigns.campaigns
    };
}

export function filterCampaigns(searchTerm){
    return {
        type: FILTER_CAMPAIGNS,
        payload: searchTerm
    };
}

export function createCampaign(data){
    return {
        type: CREATE_CAMPAIGN,
        payload: data
    };
}
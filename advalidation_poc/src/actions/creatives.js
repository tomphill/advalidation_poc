import {FETCH_CREATIVES} from "../actionTypes";

import creatives from '../dummyData/creatives.json';

export function fetchCreativesByCampaignId(campaignId){

    // filtering here from the dummy data, this won't be needed when retrieving from API
    const filteredCreatives = creatives.creatives.filter((creative)=>{
        return creative.campaignId === campaignId;
    });

    return {
        type: FETCH_CREATIVES,
        payload: filteredCreatives
    };
}
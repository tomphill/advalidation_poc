import React from 'react';
import './NewCampaignIndex.css';
import NewCampaignForm from '../../containers/campaigns/NewCampaignForm';

const NewCampaignIndex = () => {
    return (
        <div className="new-campaign-index">
            <section className="row">
                <div className="col-md-6 ml-auto mr-auto">
                    <div className="card">
                        <div className="card-body">
                            <h4 className="card-title">
                                New Campaign
                            </h4>
                            <NewCampaignForm/>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
};

export default NewCampaignIndex;

import React from 'react';
import CampaignSearchFilter from '../../containers/campaigns/CampaignSearchFilter';
import CampaignList from '../../containers/campaigns/CampaignList';
import './Campaigns.css';
import {Link} from 'react-router-dom';

const CampaignsIndex = () => {
    return (
        <div className="campaigns">
            <section className="row">
                <div className="col-md-6">
                    <h1>
                        Campaigns
                    </h1>
                </div>
                <div className="col-md-6 text-md-right">
                    <Link className="btn btn-primary btn-sm" to="/new">
                        NEW CAMPAIGN
                    </Link>
                </div>
            </section>
            <CampaignSearchFilter />
            <CampaignList />
        </div>
    );
};

export default CampaignsIndex;
import React from 'react';
import Moment from 'react-moment';

const CampaignListItem = ({campaign, onClick}) => {

    const {
        campaignName,
        campaignLastModifiedUser,
        campaignLastModifiedDateTime
    } = campaign;

    return (
        <li className="list-group-item" onClick={onClick}>
            <section className="row">
                <div className="col-md-6">
                    {campaignName}
                </div>
                <div className="col-md-2 text-muted small">
                    <i className="fa fa-image" /> 9
                </div>
                <div className="col-md-2 text-muted small">
                    {campaignLastModifiedUser}
                </div>
                <div className="col-md-2 text-muted small">
                    <Moment format="Do MMM YYYY HH:mm" date={campaignLastModifiedDateTime} />
                </div>
            </section>
        </li>
    );
};

export default CampaignListItem;
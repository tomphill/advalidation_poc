import React from 'react';
import CreativeTestsTable from './CreativeTestsTable';

const CreativeListItem = ({creative}) => {
    const {
        creativeName,
        creativeAuthor,
        creativeType,
        tests
    } = creative;

    const issues = tests.filter(test => !test.testPassed);

    return (
        <section className="card" style={{marginBottom: '10px'}}>
            <div className="card-header">
                <strong>
                    {creativeName}
                </strong>
                <div style={{display: 'flex', flexDirection: 'row'}}>
                    <div style={{flexGrow: 1}}>
                        <small>{creativeType} - {creativeAuthor}</small>
                    </div>
                    <div style={{flexGrow: 1}} className="text-right">
                        <small className="text-danger">
                            {issues.length} issues
                        </small>
                    </div>
                </div>
            </div>
            <div className="card-body">
                <CreativeTestsTable tests={tests} />
            </div>
        </section>
    );
};

export default CreativeListItem;
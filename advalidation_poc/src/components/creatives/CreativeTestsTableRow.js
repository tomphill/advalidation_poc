import React from 'react';

const CreativeTestsTableRow = ({test}) => {

    const passedTestIconClass = `fa ${test.testPassed ? 'fa-check text-success' : 'fa-close text-danger'}`;

    return (
        <tr>
            <td>
                <i className={passedTestIconClass} />
            </td>
            <td>
                {test.testName}
            </td>
            <td>
                {test.testResult}
            </td>
            <td>
                {test.specification}
            </td>
        </tr>
    );
};

export default CreativeTestsTableRow;
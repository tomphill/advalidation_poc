import React, {Component} from 'react';
import CreativesList from '../../containers/creatives/CreativesList';
import {connect } from 'react-redux';
import {fetchCampaigns} from "../../actions/campaigns";

class CreativesIndex extends Component {

    constructor(props){
        super(props);

        this.state = {
            activeCampaignId: parseFloat(this.props.match.params.campaignId)
        };

        this.props.fetchCampaigns();
    }

    render(){
        const activeCampaign = this.props.campaigns.items[this.state.activeCampaignId];

        if(!activeCampaign){
            return <div>Campaign could not be found.</div>;
        }

        return (
            <div>
                <h1>
                    {activeCampaign.campaignName}
                </h1>
                <CreativesList campaignId={this.state.activeCampaignId} />
            </div>
        );
    }
}

const mapStateToProps = ({campaigns}) => {
    return {
        campaigns
    }
};

export default connect(mapStateToProps, {
    fetchCampaigns
})(CreativesIndex);
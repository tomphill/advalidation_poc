import React, {Component} from 'react';
import CreativeTestsTableRow from './CreativeTestsTableRow';

class CreativeTestsTable extends Component {

    renderRows(){
        return this.props.tests.map((test)=>{
             return (
                 <CreativeTestsTableRow key={test.id} test={test} />
             );
        });
    }

    render(){
        return (
            <table className="table small">
                <thead>
                <tr>
                    <th />
                    <th>Test</th>
                    <th>Current</th>
                    <th>Specification</th>
                </tr>
                </thead>
                <tbody>
                {this.renderRows()}
                </tbody>
            </table>
        );
    }
}

export default CreativeTestsTable;
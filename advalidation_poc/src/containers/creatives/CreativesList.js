import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchCreativesByCampaignId} from "../../actions/creatives";
import {withRouter} from 'react-router-dom';
import CreativesListItem from '../../components/creatives/CreativesListItem';

class CreativesList extends Component {

    componentWillMount(){
        this.props.fetchCreativesByCampaignId(this.props.campaignId);
    }

    renderItems(){
        return this.props.items.map((item) => {
            return (
                <CreativesListItem key={item.id} creative={item} />
            );
        });
    }

    render(){
        return (
            <div>
                <div style={{display: !this.props.items.length ? 'none' : 'block'}}>
                    {this.renderItems()}
                </div>
                <div style={{display: this.props.items.length ? 'none' : 'block'}}>
                    There are no creatives attached to this campaign.
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({creatives: {items}}) => {
    return {
        items
    }
};

export default connect(mapStateToProps, {
    fetchCreativesByCampaignId
})(withRouter(CreativesList));
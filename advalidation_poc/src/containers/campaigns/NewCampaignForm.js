import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {createCampaign} from "../../actions/campaigns";
import { withRouter } from 'react-router-dom';

class NewCampaignForm extends Component {

    renderCampaignNameField(field){

        const {meta: {touched, error}} = field;
        const className = `form-control form-control-sm ${touched && error ? 'is-invalid' : ''}`;

        return (
            <div>
                <input required {...field.input} placeholder="Campaign name" type="text" className={className} />
                <div className="text-danger small">
                    {touched ? error : ''}
                </div>
            </div>
        );
    }

    onSubmit({campaignName}){
        this.props.createCampaign({
            id: Math.random(),
            campaignName,
            campaignLastModifiedUser: 'Jesper'
        });

        this.props.history.push("/");
    }

    render(){

        const {handleSubmit} = this.props;

        return (
            <form className="card-text" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <Field
                    name="campaignName"
                    component={this.renderCampaignNameField}
                />
                <button type="submit" style={{marginTop: '10px'}} className="btn btn-sm btn-primary btn-block">
                    CREATE
                </button>
            </form>
        );
    }
}

function validate(values){
    const errors = {};

    if(!values.campaignName){
        errors.campaignName = "Please enter a campaign name";
    }

    return errors;
}

export default reduxForm({
    validate,
    form: 'NewCampaignForm'
})(
    connect(null, {createCampaign})(withRouter(NewCampaignForm))
);
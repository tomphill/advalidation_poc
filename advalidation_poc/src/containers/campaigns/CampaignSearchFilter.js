import React, {Component} from 'react';
import './CampaignSearchFilter.css';
import {connect} from 'react-redux';
import {filterCampaigns} from "../../actions/campaigns";

class CampaignSearchFilter extends Component {

    constructor(props){
        super(props);
        this.onSearchChange = this.onSearchChange.bind(this);
    }

    onSearchChange(event){
        this.props.filterCampaigns(event.target.value);
    }

    render() {
        return (
            <div className="input-group campaign-search-filter">
                <div className="input-group-addon">
                    <i className="fa fa-search text-muted"/>
                </div>
                <input onChange={this.onSearchChange} value={this.props.searchTerm} placeholder="Filter campaigns" type="search" className="form-control form-control-sm"/>
            </div>
        );
    }
}

function mapStateToProps({campaigns: {searchTerm}}){
    return {
        searchTerm
    };
}

export default connect(mapStateToProps, {
    filterCampaigns
})(CampaignSearchFilter);
import React, {Component} from 'react';
import {connect} from 'react-redux';
import CampaignListItem from '../../components/campaigns/CampaignListItem';
import './CampaignList.css';
import {fetchCampaigns} from '../../actions/campaigns';
import _ from 'lodash';
import {withRouter} from 'react-router-dom';

class CampaignList extends Component {

    componentWillMount(){
        this.props.fetchCampaigns();
    }

    onCampaignClick(campaignId){
        this.props.history.push(`/campaign/${campaignId}`);
    }

    renderList() {
        const mappedItems = _.map(this.props.items);
        const filteredItems = this.props.searchTerm ? mappedItems.filter((campaign) => {
            return campaign.campaignName.toLowerCase().indexOf(this.props.searchTerm.toLowerCase()) > -1
        }) : mappedItems;

        return filteredItems.map(campaign => {
            return (
                <CampaignListItem
                    onClick={this.onCampaignClick.bind(this, campaign.id)}
                    key={campaign.id}
                    campaign={campaign}
                />
            );
        });
    }

    render(){
        return (
            <ul className="list-group campaign-list">
                {this.renderList()}
            </ul>
        );
    }
}

function mapStateToProps({campaigns: {items, searchTerm}}){
    return {
        items,
        searchTerm
    }
}

export default connect(mapStateToProps, {
    fetchCampaigns
})(withRouter(CampaignList));
import React from 'react';
import ReactDOM from 'react-dom';
import reducers from './reducers';
import registerServiceWorker from './registerServiceWorker';
import { createStore } from "redux";
import { Provider } from "react-redux";
import {Route, BrowserRouter, Switch} from 'react-router-dom';

import CreativesIndex from './components/creatives/CreativesIndex';
import CampaignsIndex from './components/campaigns/CampaignsIndex';
import NewCampaignIndex from './components/campaigns/NewCampaignIndex';

ReactDOM.render(
    <Provider store={createStore(reducers)}>
        <BrowserRouter>
            <div>
                <Switch>
                    <Route path="/campaign/:campaignId" component={CreativesIndex} />
                    <Route path="/new" component={NewCampaignIndex} />
                    <Route path="/" component={CampaignsIndex} />
                </Switch>
            </div>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);

registerServiceWorker();

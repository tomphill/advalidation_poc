import {
    FETCH_CAMPAIGNS,
    CREATE_CAMPAIGN,
    FILTER_CAMPAIGNS
} from "../actionTypes";
import _ from 'lodash';

const INITIAL_STATE = {
    items: {},
    searchTerm: ''
};

export default function(state = INITIAL_STATE, action){
    switch(action.type){
        case FETCH_CAMPAIGNS:
            return {...state, ...{items: {..._.mapKeys(action.payload, "id"), ...state.items}}};
        case CREATE_CAMPAIGN:
            return {...state, ...{items: {..._.mapKeys([action.payload], "id"), ...state.items}}};
        case FILTER_CAMPAIGNS:
            return {...state, ...{searchTerm: action.payload}};
        default:
            return state;
    }
}
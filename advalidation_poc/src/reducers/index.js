import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import CampaignsReducer from './campaigns';
import CreativesReducer from './creatives';

export default combineReducers({
    campaigns: CampaignsReducer,
    creatives: CreativesReducer,
    form: formReducer
});
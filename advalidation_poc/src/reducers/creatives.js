import {FETCH_CREATIVES} from "../actionTypes/index";

const INITIAL_STATE = {
    items: []
};

export default function(state = INITIAL_STATE, action){
    switch(action.type){
        case FETCH_CREATIVES:
            return {...state, ...{items: action.payload}};
        default:
            return state;
    }
}